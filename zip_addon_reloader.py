# ##### BEGIN GPL LICENSE BLOCK ######
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

import shutil
import os
from os import path
import sys
import subprocess

import bpy
from bpy.types import Panel, Operator, Scene, PropertyGroup
from bpy.props import StringProperty, PointerProperty, BoolProperty

bl_info = {
    "name": "Zip Addon Reloader",
    "author": "Jake Dube",
    "version": (1, 1),
    "blender": (3, 1, 0),
    "location": "3D View > Tools > Zip Addon Reloader",
    "description": "An addon to reload zip addons.",
    "wiki_url": "",
    "category": "Development",
    }


class ZipAddonReloaderPanel(Panel):
    bl_label = "Zip Addon Reload"
    bl_idname = "VIEW3D_PT_tools_ZIPADDONRELOADER"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_context = "objectmode"
    bl_category = 'Tool'

    def draw(self, context):
        zip_reload = context.scene.zip_reload
        layout = self.layout
        
        row = layout.row()
        row.scale_y = 1.5
        row.operator("zip_reload.reload_addon", icon="NODE")
        layout.prop(zip_reload, "addon_dir")
        layout.prop(zip_reload, "zip_dir")


def install_addon():
    zip_reload = bpy.context.scene.zip_reload

    # install addon from zip file
    bpy.ops.preferences.addon_install(
        'EXEC_DEFAULT', 
        overwrite=True, 
        filepath=path.join(
            zip_reload.zip_dir, 
            zip_reload.addon_name + ".zip")
       )


def enable_addon():
    zip_reload = bpy.context.scene.zip_reload
    bpy.ops.preferences.addon_enable(module=zip_reload.addon_name)


def build_zip():
    zip_reload = bpy.context.scene.zip_reload

    # make the add-on name that of the project's directory
    # e.g., if we are given:
    # /home/user/addon_directory/ or /home/user/addon_directory
    #
    # we should get addon_directory either way
    addon_name = path.basename(zip_reload.addon_dir)
    addon_dir = path.dirname(zip_reload.addon_dir)
    if not addon_name:  
        # there was an extra slash at the end, remove it
        clean_path = path.split(zip_reload.addon_dir)[0]
        addon_name = path.basename(clean_path)
        addon_dir = path.dirname(clean_path)

    # store this since it is needed by the installer function
    zip_reload.addon_name = addon_name
    bpy.ops.wm.save_mainfile()

    # build the zip file
    zipf = path.join(zip_reload.zip_dir, addon_name)
    shutil.make_archive(
        path.join(zip_reload.zip_dir, addon_name), 
        'zip', 
        addon_dir, 
        addon_name)


def remove_old_addon():
    zip_reload = bpy.context.scene.zip_reload

    # disable and remove addon
    bpy.ops.preferences.addon_disable(module=zip_reload.addon_name)
    bpy.ops.preferences.addon_remove(module=zip_reload.addon_name)


def restart_blender():
    # snippet to automatically install and enable addon
    snippet = "import zip_addon_reloader as zar;" \
              "zar.install_addon();" \
              "zar.enable_addon()"

    # queue Blender to start back up
    # NOTE: the snippet must come after the file so the file is open
    subprocess.Popen([sys.argv[0], 
                      bpy.data.filepath, 
                      "--python-expr", snippet])

    # actually quit Blender
    bpy.ops.wm.quit_blender()


class ZipAddonReloaderOperator(Operator):
    bl_label = "Reload Addon"
    bl_idname = "zip_reload.reload_addon"
    bl_description = "Reload addon by building zip file, removing old " \
                     "version, restarting Blender, installing built zip " \
                     "file, and enabling the addon"
    bl_options = {'REGISTER', 'UNDO'}

    cont: BoolProperty()

    def invoke(self, context, event):
        # check if the current file is saved before doing anything destructive
        if bpy.data.is_dirty:
            self.cont = False
            return context.window_manager.invoke_props_dialog(self, width=500)
        else:
            self.cont = True
            return self.execute(context)
    
    def draw(self, context):
        layout = self.layout
        layout.prop(self, "cont", text="Continue (file will be saved)")

    def execute(self, context):
        if not self.cont:
            return {'CANCELLED'}
        print("Reloading addon...")

        build_zip()
        remove_old_addon()
        restart_blender()

        return{'FINISHED'}


def make_absolute(key):
    """Ensure that the path stored in zip_reload[key] is absolute"""

    zip_reload = bpy.context.scene.zip_reload
    if key in zip_reload and zip_reload[key].startswith('//'):
        # must use a[x] notation instead of a.x so it doesn't 
        # trigger update callbacks infinitely
        zip_reload[key] = path.abspath(bpy.path.abspath(zip_reload[key]))


class ZipAddonReloaderPropertyGroup(PropertyGroup):
    zip_dir: StringProperty(
        name="Zip Directory", 
        default="", 
        subtype='FILE_PATH',
        update=lambda self, context: make_absolute('zip_dir'))

    addon_dir: StringProperty(
        name="Addon Directory", 
        default="", 
        subtype='FILE_PATH',
        update=lambda self, context: make_absolute('addon_dir'))

    addon_name: StringProperty(
        name="Addon Name",
        default="")


classes = [ZipAddonReloaderPanel, 
           ZipAddonReloaderOperator, 
           ZipAddonReloaderPropertyGroup,
          ]


def register():
    for i in classes:
        bpy.utils.register_class(i)

    Scene.zip_reload = PointerProperty(type=ZipAddonReloaderPropertyGroup)


def unregister():
    for i in classes:
        bpy.utils.unregister_class(i)

    del Scene.zip_reload


if __name__ == "__main__":
    register()

