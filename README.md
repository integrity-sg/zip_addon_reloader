# zip_addon_reloader
Reloads .zip add-ons for Blender for faster development.

[Check the wiki documentation for more information](https://gitlab.com/integrity-sg/zip_addon_reloader/wikis/home)
